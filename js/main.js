$(".activities_slider").owlCarousel({
    loop: true,
    navigation: true,
    navigationText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
    ],
    dots: true,
    autoplay: false,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        768: {
            items: 1,
            nav: true
        },
        992: {
            items: 1,
            nav: true,
            loop: true
        }
    }
});
$(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
$(".owl-next").html('<i class="fa fa-chevron-right"></i>');

function getColor() {
    var color = $("#color").val();
    $(".profile_cover_image").css("background", color);

}
$(document).ready(function() {
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
});

$(document).ready(function() {
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#bgimagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#bgimagePreview').hide();
                $('#bgimagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#bgimageUpload").change(function() {
        readURL(this);
    });
});
$('.multi-field-wrapper').each(function() {
    var $wrapper = $('.multi-fields', this);
    $(".add-field", $(this)).click(function(e) {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });
    $('.multi-field .remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();

    });
});
$(function() {
    $('.bs-timepicker').timepicker();

});
$(document).ready(function() {
    $('.js-edit, .js-save').on('click', function() {
        var $form = $(this).closest('form');
        $form.toggleClass('is-readonly is-editing');
        var isReadonly = $form.hasClass('is-readonly');
        $form.find('input,textarea').prop('disabled', isReadonly);
    });
    $("input[type='checkbox'].select_check").change(function() {
        if ($(this).is(":checked")) {
            $(this).parent().addClass("select_all");
        } else {
            $(this).parent().removeClass("select_all");
        }
    });
});